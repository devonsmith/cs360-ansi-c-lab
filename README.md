# README #

This is a project for CS360 at Western Oregon University.
The description of this project can be found in the file Lab3.pdf
This application will find the MP3 frame in an MP3 file and determine the file's size, bitrate, frequency, and determine if the file has a copyright flag and if the file is a copy.

### What is this repository for? ###

* CS360 - ANSI C Lab
* Version 1
* Devon Smith

### How do I get set up? ###

* Will compile using gcc on linux or MSVC on Windows.
* Compiles with: gcc -std=c89 -pedantic -Wall Source.c