/*****************************************************************************
*  CS360 - Lab 3
*  C Data Types Lab
*  Devon Smith
*  Western Oregon University
*  Fall 2016
*
* Project Description: Extend a pre-existing application in ANSI C 89 and 
* extend it to find the following attributes: MPEG Type, Bit rate, Frequency,
* Copyright, and Original.
*
******************************************************************************/


#include <stdio.h>
/* Since this uses exit(int status) we need to either define it or use the code 
in the standard library. */
#include <stdlib.h>
/* Needed for the function strrch which will find the last occurrence of a 
character in a string. This allows the finding of the extension. Also will use 
strcmp to compare the located string in the file name against the expected 
extension.*/
#include <string.h>

/*CPP Directives*/
/* Define a Megabyte in Bytes. */
#define MiB 1048576
/* Define max file size */
#define MAX_FILE_SIZE MiB*20 /* 20 Megabytes */

/*
NOTE: When I was given the lab the file size limit was set to 10 MiB, I only
have a single MP3 in my collection smaller than that value. I have increased
this value to 20 MiB.*/

/*****************************************************************************

Project TODO list:
* Try compiling. From the command line: gcc Lab3.c Fix that. 
  ALL ERRORS CORRECTED. OK

* NOTE: The comments in this file were not C89 compliant. The // comments are 
  a C++ comment style that is not in the C89 standard. I updated the comments 
  to match the C89 comment standard. OK

* Now that it is compiling, you remember that C doesn't do a lot of type 
  checking. You better compile with all the warnings turned on: 
  gcc -Wall Lab3.c. Aha, fix those. 
  Compiled against VCC, ICC, and GCC in C89 mode.
  GCC command used: gcc -std=c89 -Wall -pedantic Source.c OK

  Fix Type errors: OK
  Fix printf statement errors: OK
  Fix comments: OK

Note: I changed the name of the source file for my environment.

* The code uses goto!!! Quaint, but let's fix that.
  Created a function that closes the application. This takes an argument (a 
  pointer to a struct). This is for expansion of the application functionality
  (getting rid of the global pointer).

Gotos killed: OK

* Lose constants (lines 29 and 35). Those should be #define'd or declared with
  constants. What in the heck is 10485760?
  10 Megabytes * 1024 Kilobytes/1 Megabyte * 1024 Bytes/Kilobytes

  Define in the preprocessor: OK

* Fix that so it prints out the file size to 2 decimal points.
  Converted value to a float before division and changed the string format in
  the printf statement to %0.2f

  Output on test file: 5.48 MB: OK

* Memory Leak!! The memory allocated on line 37 isn't ever freed. Fix that.
  Added free(data) to the code to free the malloc'd memory space before 
  application exit. This can be moved to the appropriate location once all the
  code has been written

  Add free(): OK


* All the code is in the main function. Let's modularize and put things in 
  separate functions. That stuff at the top (lines 7 - 19) just gets the file 
  name and opens a file. Put that into a function named initialize. 
  Lines 21 - 44 only read in the file into memory, put that into a function 
  called readFile. We could make all variables global to do that? No way, 
  thatâ€™s a hack. We can get by with only one global variable: File * fp, 
  everything else needs to be passed in as an argument or returned as the 
  return value. If we have a lot to pass in and out, maybe we could just pass
  in a pointer to a struct? Yeah, that would be cool. Probably get extra 
  credit for that one.

  move 7-19 to function to open_file(): OK
  move lines 21-44 to readFile(): OK
  Build a struct and pass it to initialize() and readFile(): OK


* Looks good now. Compile and run (./a.out song.mp3) and see how it goes. Grab 
  some more mp3â€™s to try it out. OK

*****************************************************************************/

/*****************************************************************************
Other TODO:

* Move Free() to an appropriate location: OK
* Determine the MPEG Level of the File: OK
* Determine the MPEG Layer of the file: OK
* Determine the bitrate of the file: OK
* Determine the frequency rate of the file: OK
* Check the file type and extension before attempting to analyze: OK

*****************************************************************************/

/*****************************************************************************
Grading Rubric Items

Code Compiles with warnings turned on: OK
Goto removed: OK
Constants defined: OK (As preprocessor directives: Lines 25 - 29)
File size prints out to two decimal places: OK
Memory leak fixed: OK (Free(): Line 535)
initialize function created: OK
readFile function created: OK
One global variable: OK
print out include file size: OK (Line: 375)
print out includes bitrate: OK (Checked files against other applications)
print out includes frequency: OK (Checked files against other applications)
print out copyright information: OK (Checked files against other applications)
Print out includes if the file is original: OK (Checked files against other 
applications)
Print out only if the file is an MPEG Layer III file: OK

*****************************************************************************/

/*Struct for storing file and data properties*/
typedef struct {
	/* the number of arguments passed to this application */
    int * arg_count; 
	/* A pointer to the character array of the arguments passed to this 
	   application*/
    char ** arg_vals; 
	/* a pointer to the file data start location in memory */
    unsigned char * file_data; 
	/* The size of the file */
    long file_size; 
    /* add content for the file information */
}file_data;


/* Function Definitions */
/*Initialize all the information by opening the file if possible.*/
FILE * initialize(file_data * fd); 
/* read the contents of the file, determine size, find data. */
void readFile(file_data * fd); 
/* Function to find the MP3 frame in the File * steam */
unsigned char * find_frame(file_data *fd); 
void exitApplication(file_data * fd);

/*Global Variable as Required by the Assignment*/
FILE * fp; /* NOTE: This could also be placed in the file struct*/

int main(int argc, char ** argv)
{
    /* Create an instance of the struct. */
    file_data fdata;

    /* Setup the arg count and arg values in the file data struct.*/
    fdata.arg_count = &argc;
    fdata.arg_vals = &*argv;

    /* get the file pointer */
    fp = initialize(&fdata);

    /* read the file */
    readFile(&fdata);

    exitApplication(&fdata);

    /* Since this is the main function we need to return *something* */
    /* We're never going to get this far but all mains must have the return
	type int.*/
    return 0;
}

FILE * initialize(file_data * fd) {

    /* The return for the pointer*/
    FILE * ret;
    /* a pointer for the file extension start*/
    char * file_ext;

    /* lower case extension for MP3 files.*/
    char extension[] = ".mp3";
    /* upper case extension for MP3 files. */
    char uextension[] = ".MP3";

    /* Open the file given on the command line */
    if (*fd->arg_count != 2)
    {
        printf("Usage: %s filename.mp3\n", fd->arg_vals[0]);
        return 0;
    }

    /* Get the extension of the file. find the last period on the file name.*/
    file_ext = strrchr(fd->arg_vals[1], '.');
    
	/* Do we use more memory by storing another string or more processor time 
	by converting the string's case?*/
    
	/* In this situation I'm going to use a little more memory.*/
    /* did we find the extension in the file?*/
    if (file_ext) {
        /* if the extension was found in the file, was it MP3 or mp3?*/
        if (!(strcmp(file_ext, &extension[0]) == 0 || strcmp(file_ext, &uextension[0]) == 0)) {
            printf("Please specify a valid MPEG 1/2 Layer III File.\n\n");
            printf("Usage: %s filename.mp3\n", fd->arg_vals[0]);
            exitApplication(fd);
        }
    }

    /* Preprocessor directives for this function: */
#ifdef _WIN64
	/* fopen_s returns a error string when it fails. This defined from 
	C Preprocessor Documentation, MSDN., Retrieved November 09, 2016*/
    errno_t err; 
    err = fopen_s(&ret, fd->arg_vals[1], "rb");
#elif defined _WIN32
	/* fopen_s returns a error string when it fails. This defined from 
	C Preprocessor Documentation, MSDN., Retrieved November 09, 2016*/
    errno_t err; 
    err = fopen_s(&ret, fd->arg_vals[1], "rb");
#else
    ret = fopen(fd->arg_vals[1], "rb");
#endif

#ifdef _WIN64
    if (err != 0) {
        printf("Can't open file %s\n", fd->arg_vals[1]);
        exitApplication(fd);
    }
#elif defined _Win32
    if (err != 0) {
        printf("Can't open file %s\n", fd->arg_vals[1]);
        exitApplication(fd);
    }
#endif
    /* Failsafe in case the file couldn't be opened.*/
    if (!ret)
    {
        printf("Can't open file %s\n", fd->arg_vals[1]);
        exitApplication(fd);
    }
    return ret;
}

void readFile(file_data * fd) {
    /* Declarations */
    size_t bytesRead;
    unsigned char * frame_start = find_frame(fd);
    unsigned char * frame2 = frame_start + 1;
    unsigned char * frame3 = frame2 + 1;
    unsigned char * frame4 = frame3 + 1;
    /* Bit rate variables */
    int version;
    int bitrate;
    /* Frequency variables */
    int freq;
    int freq_val;
    /* Is it copyrighted? */
    int copyright;
    /* is it a copy? */
    int copy;

    /* How many bytes are there in the file?  If you know the OS you're */
    /* on you can use a system API call to find out.  Here we use ANSI 
	standard */

    /* Find the end of the file */

	/* go to 0 bytes from the end */
    fseek(fp, 0, SEEK_END);
	/* how far from the beginning? */
    fd->file_size = ftell(fp);
	/* go back to the beginning */
    rewind(fp);

	/* 1024 B in KiB, 1024 KiB in MiB * 10 = 10485760 */
    if (fd->file_size < 1 || fd->file_size > MAX_FILE_SIZE) 
    {
        printf("File size is not within the allowed range\n");
        /* Here lies, goto... he was loved and then we killeded him. */
        /* goto END; X_X */
        /* Clean up */
        exitApplication(fd);
    }

    /*************************************************************************

    The may not be the best way to load the file into memory. This loads the 
	entire file into memory. This is a requirement of this lab. Another 
	possible approach would be to use fgetc to get the characters out of the 
	file. However, since this is an instructional problem loading the 
	information into a malloc'd space gives us lots of time	with pointers.<3

    *************************************************************************/


    /* Allocate memory on the heap for a copy of the file*/

    fd->file_data = (unsigned char *)malloc(fd->file_size);

    /* Read it into our block of memory*/
    bytesRead = fread(fd->file_data, sizeof(unsigned char), fd->file_size, fp);

    if (bytesRead != fd->file_size)
    {
        /*
        The format string %d was defined but variadic argument 1 was of type 
		size_t. Used %zd instead. zd is not valid in C90, so I used %ld 
		instead, and cast the value as a long integer. Depending on the compiler 
		this will be a minimum of 2.14 GiB, however may be longer in some compilers
		This should not out run the limit set in the constant for file length 
		(20 MiB).
        */
        printf("Error reading file. Unexpected number of bytes read: %ld\n", (long)bytesRead);

        /* Here lies, goto... he was loved and then we killeded him.*/
        /* goto END; X_X */
        /* EXIT TIME!*/
        exitApplication(fd);
    }

    /* Get the start of the frame location. */
    frame_start = find_frame(fd);

    if (!frame_start) {
        printf("MPEG Header Frame not found. Is this a valid MPEG FILE?");
        exitApplication(fd);
    }

    /* I'm going to break these into block, one for each octet */
    frame2 = frame_start + 1;
    frame3 = frame2 + 1;
    frame4 = frame3 + 1;

    /* DIAGNOSTIC PRINTOUT */
    /*printf("%d, %d, %d, %d\n", *frame_start, *frame2, *frame3, *frame4 );*/

    /* File Format Check*/
	/* Program will not detect Version 2.5 files */
    version = ((*frame2 & 8) == 8) ? 1 : 2; 

    /* MPEG Layer III check*/
    /* frame2 AND 0x0A (0000 1010) */
    /* Is it a layer III file? */
    if ((*frame2 & 6) == 2) {
        /* Print a header block for our file summary.*/
        printf("\n****************************************************************************\n");
        printf("     %s summary\n", fd->arg_vals[1]);
        printf("****************************************************************************\n\n");
        /* end of header block*/
        printf("File Type: MPEG %d Layer III File.\n", version);
    }
    else {
        printf("Not a valid MPEG 1/2 Layer III file.");
        exitApplication(fd);
    }

    /* format to x.xx.*/
	/* 1024 B in KiB, 1024 KiB in MiB = 1048576*/
    printf("File size: %0.2f MiB\n", (float)fd->file_size / MiB); 
    /* Bit Rate ( & 1111 0000 : 240)*/
    bitrate = (*frame3 & 240);
    /* if this was a MPEG 1 Layer III file */
    if (version == 1) {
        switch (bitrate) {
            /*0001 0000 0x10 16 */
        case 16:
            bitrate = 32;
            break;
            /*0010 0000 0x20 32 */
        case 32:
            bitrate = 40;
            break;
            /*0011 0000 0x30 48 */
        case 48:
            bitrate = 48;
            break;
            /*0100 0000 0x40 64 */
        case 64:
            bitrate = 56;
            break;
            /*0101 0000 0x50 80 */
        case 80:
            bitrate = 64;
            break;
            /*0110 0000 0x60 96 */
        case 96:
            bitrate = 80;
            break;
            /*0111 0000 0x70 112 */
        case 112:
            bitrate = 96;
            break;
            /*1000 0000 0x80 128 */
        case 128:
            bitrate = 112;
            break;
            /*1001 0000 0x90 144 */
        case 144:
            bitrate = 128;
            break;
            /*1010 0000 0xA0 160 */
        case 160:
            bitrate = 160;
            break;
            /*1011 0000 0xb0 176 */
        case 176:
            bitrate = 192;
            break;
            /*1100 0000 0xc0 192 */
        case 192:
            bitrate = 224;
            break;
            /*1101 0000 0xd0 208 */
        case 208:
            bitrate = 256;
            break;
            /*1110 0000 0xe0 224 */
        case 224:
            bitrate = 320;
            break;
        default: bitrate = 0;
        }
    }
    /* If it's an MPEG 2 Layer III file, there are different bit rates.*/
    else if (version == 2) {
        switch (bitrate) {
            /*0001 0000 0x10 16 */
        case 16:
            bitrate = 8;
            break;
            /*0010 0000 0x20 32 */
        case 32:
            bitrate = 16;
            break;
            /*0011 0000 0x30 48 */
        case 48:
            bitrate = 24;
            break;
            /*0100 0000 0x40 64 */
        case 64:
            bitrate = 32;
            break;
            /*0101 0000 0x50 80 */
        case 80:
            bitrate = 40;
            break;
            /*0110 0000 0x60 96 */
        case 96:
            bitrate = 48;
            break;
            /*0111 0000 0x70 112 */
        case 112:
            bitrate = 56;
            break;
            /*1000 0000 0x80 128 */
        case 128:
            bitrate = 64;
            break;
            /*1001 0000 0x90 144 */
        case 144:
            bitrate = 80;
            break;
            /*1010 0000 0xA0 160 */
        case 160:
            bitrate = 96;
            break;
            /*1011 0000 0xb0 176 */
        case 176:
            bitrate = 112;
            break;
            /*1100 0000 0xc0 192 */
        case 192:
            bitrate = 128;
            break;
            /*1101 0000 0xd0 208 */
        case 208:
            bitrate = 144;
            break;
            /*1110 0000 0xe0 224 */
        case 224:
            bitrate = 160;
            break;
        default: bitrate = 0; /* Bit rate can be zero in some strange cases. */
        }
    }
    /*Print the bit rate */
    if (bitrate > 0) {
        printf("Bit rate: %d Kbps\n", bitrate);
    }
    /* There are some special cases where the bit rate might be continuously encoded and not available. */
    else {
        printf("Bit rate: Constant Undefined Bit rate.");
    }

    /* Frequency */
    freq = *frame3 & 12;
    /* find the frequency of the file based on know encoding values.*/
    freq_val = (freq == 0) ? 44100 : (freq == 4) ? 48000 : (freq == 8) ? 32000 : 0;
    if (freq_val > 0) { printf("Playback Frequency: %d Hz\n", freq_val); }
	/* 
	There is a reserved type that may result in a number out of range: 0 
	000000 is the reserved for continuously encoded files.
	*/
    else { printf("Playback Frequency: Reserved Type\n"); } 
                                                            
    /*Copyright*/
    copyright = *frame4 & 8;
    /*Is the copyright flag set? (this is not very common) */
    if (copyright == 8) { printf("Copyright: Yes\n"); }
    else { printf("Copyright: No\n"); }

    /* Copy bit */
    copy = *frame4 & 4;
    /* Is the file a copy?*/
    if (copy == 4) { printf("File is original: Yes\n"); }
    else { printf("File is original: No\n"); }

    /* Free! Release all that malloc'd memory space. */
    free(fd->file_data);

    /* A few new lines for formatting.*/
    printf("\n\n");
}

/* MP3 Frame Finder*/

/*****************************************************************************
POSSIBLE STATES FOR UNSIGNED CHARS
Looking for bit pattern: 0xFFE
Possible bit pattern	| Hex Value | Numeric Values | Offset
1111 1111 1110 0000		| 0xFFE0	| 255, 224		 | 0
0111 1111 1111 0000		| 0x7FF0	| 127, 240		 | 1
0011 1111 1111 1000		| 0x3FF8	| 63, 248		 | 2
0001 1111 1111 1100		| 0x1FFC	| 31, 252		 | 3
0000 1111 1111 1110		| 0x0FFE	| 15, 254		 | 4
0000 0111 1111 1111		| 0x07FF	| 7, 255		 | 5

offset values to calculate the start of the frame:
pointer + offset

Technically peaking the bit pattern for the MP3 sync bits can be 0xFFE or 
0xFFE but according to the documentation from WIkipedia and from the lab the 
pattern 0xFFF. So we're going to look for that instead.

Looking for bit pattern: 0xFFF
Possible bit pattern	| Hex Value | Numeric Values | Offset
1111 1111 1111 0000		| 0xFFF0	| 255, 240		 | 0
0111 1111 1111 1000		| 0x7FF8	| 127, 248		 | 1
0011 1111 1111 1100		| 0x3FFC	| 63, 252		 | 2
0001 1111 1111 1110		| 0x1FFE	| 31, 254		 | 3
0000 1111 1111 1111		| 0x0FFF	| 15, 255		 | 4

offset values to calculate the start of the frame:
pointer + offset

******************************************************************************/

/* This function will take the data and look for the location of the MP3 frame
location and return the integer representation of the offset location of the 
MP3 frame in the character stream.*/
unsigned char * find_frame(file_data *fd) {
    /* return the number of characters to jump into the file space.*/
    int i, j;
    /*
    To look for offset values we're going to go through the characters looking
    for them then move over a bit, rinse, repeat. The odds of needing to go 
    through this more than once is extremely low. There should be at least one 
	MP3 frame that meets the byte boundary within 1 million+ bytes.
    */
    for (i = 0; i < 8; ++i) {
        /* move over by 1 char, this could also be done with a _Bool in C99 or 
		GNU C and move it one byte at a time.*/
        unsigned char * current_char = fd->file_data + i; 
        unsigned char * next_char = fd->file_data + 1;
        /* 
		Could use a do loop here since I'm not doing anything with an iterator.
		*/
        for (j = 0; j < fd->file_size - 1; ++j) {
            /* 
			If the current character is 0xFF and the next character is 
			between 0xF0 and 0xFF (inclusive)
			*/
            if (*current_char == 255) {
                /* We found the start of the frame return an offset integer.*/
                if (*next_char >= 240) {
                    return current_char;
                }
            }
            /* If next char is 0xFF we can check against it.*/
            if (*next_char == 255) {
                /* Make the next current the one we need to check against.*/
                current_char = next_char;
                /* jump the next char forward one instead of two chars.*/
                next_char += 1;
            }
            /* Otherwise we can safely skip over these bits. Since there is no
			chance of matching with them.*/
            else {
                /*Jump over two chars for each character pointer.*/
                current_char += (sizeof(_Bool));
                next_char = current_char + 1;
                /* advance the i value an extra time, since this will move the
				current pointer forward two units.*/
                ++j;
            }
        }
    }
    /* If we don't find an MP3 frame, return a null pointer.*/
    return NULL;
}

/*
May not need to pass file data to this file, since I moved fp to a global as
required in the problem statement. Possible cleanup? Leave in place for 
possible extensibility?
*/
void exitApplication(file_data * fd) {
    /* If a file is open */
    if (fp) {
		/* close the file */
        fclose(fp); 
    }
    /* exit the application */
    exit(EXIT_SUCCESS);
}